@extends('layouts.app')

@section('content')
    @include('notifications.alerts')

    <h5>Создать </h5>
    <form action="{{route('posts.store')}}" enctype="multipart/form-data"  method="POST">
        @csrf

        <div class="form-group">
            <label>img</label>
            <input type="file" class="form-control"  name="image">
            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Описание</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}</textarea>
        </div>
        <button class=" btn btn-success" type="submit">Загрузить</button>
    </form>
@endsection

