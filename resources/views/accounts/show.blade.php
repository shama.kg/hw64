@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <div class="row">
        <div class="col-lg-4 d-flex justify-content-around p-5">
            @if($user->avatar)
            <img src="{{asset('/storage/'.$user->avatar) }}" class="rounded rounded-circle " height="200px">
            @else
                <img src="{{asset('default.jpg') }}" class="rounded rounded-circle " height="200px">
            @endif
        </div>
        <div class="col-lg-8 p-5">
            <h2>{{ $user->name }}    </h2>
            <form action="{{route('account.following.store', ['account' => $user])}}" method="post">
                @csrf


                    @if(auth()->user()->following->contains($user->id))
                    <button type="submit" class="btn btn-outline-secondary">Отписатся </button>
                    @else
                    <button type="submit" class="btn btn-outline-primary">Подписаться </button>

                    @endif

            </form>
            <div class="d-flex fs-5 ">
                <p class="pe-5 "> <b>{{$count}}</b> posts </p>
                <p class="pe-5"> follower <b>{{$users}}</b> </p>
                <p> following  <b>{{$following}}</b></p>
            </div>
            @can('create-post',$user)
            <a class="btn btn-outline-dark " href="{{route('posts.create', ['user' => $user])}}">
                Добавить пост
            </a>
            @endcan
        </div>
    </div>
    <div class="row">

        @foreach($posts as $post)

            <div class="col-3  border ">
                <a href="{{route('posts.show', ['post'=>$post])}}">
                    <img class="w-100 h-100" src="{{asset('/storage/'.$post->image) }}" >
                </a>
            </div>


        @endforeach
    </div>



@endsection
