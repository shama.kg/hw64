@extends('layouts.app')

@section('content')

@foreach($posts as $post)
<div class="Instagram-card">
    <div class="Instagram-card-header">
        @if($post->user->avatar)
        <img src="{{asset('/storage/'.$post->user->avatar) }}" class="Instagram-card-user-image">
        @else
            <img src="{{asset('default.jpg') }}" class="Instagram-card-user-image">
            @endif
            <a class="Instagram-card-user-name" href="{{route('account.show', ['account'=> $post->user->id])}}"> {{ $post->user->name }}</a>
        <div class="Instagram-card-time"> 1 sem </div>
    </div>

    <div class="Instagram-card-image p-5">
        <a href="{{route('posts.show', ['post'=> $post])}}">
            <img src= "{{asset('/storage/'.$post->image) }}"  height=500px/>
        </a>
    </div>

    <div class="Instagram-card-content">
        <form action="{{route('posts.likes.store', ['post' =>$post])}}" method="post">
            @csrf
            <button type="submit" class="border-0 bg-transparent">
                @auth()
                    <span>{{$post->liked_count}}</span>
                @if(auth()->user()->likedPosts->contains($post->id))
                        <img style="width: 33px; height: 33px"
                             src="{{asset('img/like.png')}}"
                             alt="">
                    @else
                        <img style="width: 33px; height: 33px"
                             src="{{asset('img/2.jpg')}}"
                             alt="">
                    @endif

                @endauth
            </button>
        </form>

        <p><a class="Instagram-card-content-user"
                href="{{route('account.show', ['account'=> $post->user->id])}}">{{ $post->user->name }}</a>
            {{$post->description}}</p>

    </div>
    <hr>

</div>
@endforeach
@endsection
