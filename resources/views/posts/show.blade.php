@extends('layouts.app')

@section('content')
    @include('notifications.alerts')

    <div class="Instagram-card">
    <div class="Instagram-card-header">
        <img src="{{asset('/storage/'.$post->user->avatar) }}" class="Instagram-card-user-image">
        <a class="Instagram-card-user-name" href="{{route('account.show', ['account'=> $post->user->id])}}"> {{ $post->user->name }}</a>
        <div class="Instagram-card-time"> <a href="{{route('account.show', ['account'=> $post->user])}}" class="fs-2">x</a> </div>
    </div>

    <div class="Instagram-card-image p-5">
        <img src= "{{asset('/storage/'.$post->image) }}"  height=500px/>
    </div>

    <div class="Instagram-card-content">
        <form action="{{route('posts.likes.store', ['post' =>$post])}}" method="post">
            @csrf
            <button type="submit" class="border-0 bg-transparent">
                <span>{{$post->liked_count}}</span>
            @auth()
                    @if(auth()->user()->likedPosts->contains($post->id))
                        <img style="width: 33px; height: 33px"
                             src="{{asset('img/like.png')}}"
                             alt="">
                    @else
                        <img style="width: 33px; height: 33px"
                             src="{{asset('img/2.jpg')}}"
                             alt="">
                    @endif

                @endauth
            </button>
        </form>
        <p><a class="Instagram-card-content-user"
              href="{{route('account.show', ['account'=> $post->user->id])}}">{{ $post->user->name }}</a>
            {{$post->description}}</p>


        <p class="comments">Комментарии</p>
    @foreach($comments as $comment)
        <br><a class="user-comment" href="">{{$comment->user_id}}</a> {{$comment->comment}}</br>
        @can('delete-comment', $comment)
            <form action="{{route('posts.comments.destroy', ['post'=> $post ,'comment'=>$comment])}} " method="POST" >
                @csrf
                @method('DELETE')
                <button type="submit"> delete</button>
            </form>
            @endcan
        <hr>
        @endforeach

    </div>

    <div class="Instagram-card-footer">
        <a class="footer-action-icons"href="#"><i class="fa fa-heart-o"></i></a>
        <form action="{{route('posts.comments.store', ['post' => $post])}}" method="post">
            @csrf
            <input name="comment" class="comments-input" type="text" placeholder="Добавить комментарии..."/>
            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
            <input type="hidden" name="post_user_id" value="{{$post->user_id}}">

            <button type="submit">добавить</button>

        </form>

        <a class="footer-action-icons"href="#"><i class="fa fa-ellipsis-h"></i></a>
    </div>

</div>
@endsection
