<?php

namespace App\Orchid\Screens\Comments;

use App\Models\Comment;
use App\Models\User;
use App\Orchid\Layouts\CommentListLayout;
use http\Env\Request;
use Orchid\Alert\Alert;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'comments' => Comment::paginate()
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Comment';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Create new')
                ->icon('bs.pencil')
                ->route('platform.comments.edit')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            CommentListLayout::class
        ];
    }

}
