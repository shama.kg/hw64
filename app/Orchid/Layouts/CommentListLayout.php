<?php

namespace App\Orchid\Layouts;

use App\Models\Comment;
use App\Models\User;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
//            TD::make()
//                ->render(function (User $user){
//                    return CheckBox::make('users[]')
//                        ->value($user->id)
//                        ->placeholder($user->name)
//                        ->checked(false);
//                }),
//            TD::make('approve', 'Approve')
//                ->render(function (Comment $comment) {
//                    return Button::make('approve')
//                    ->icon('bs.check-circle')
////                    ->canSee(!$this->comment->approve)
//                    ->method('set', [$comment]);
//
//                }),

//
//            Button::make(__('Delete'))
//                ->icon('bs.trash3')
//                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
//                ->method('remove'),


            TD::make('approve', 'Approve')->render(function (Comment $comment) {
                return Link::make($comment->approve?'true':'false')
                    ->route('platform.comments.edit', $comment);
            }),
            TD::make('comment', 'Comment')->width('200px')
                ->render(function (Comment $comment) {
                    return Link::make($comment->comment)
                        ->route('platform.comments.edit', $comment);
                }),

            TD::make('user_id', 'User-id'),
            TD::make('post_id', 'Post-id'),
            TD::make('created_at', 'Created'),
            TD::make('updated_at', 'Last edit'),
        ];


    }
}
