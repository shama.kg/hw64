<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use Illuminate\Http\Request;

class AccauntsFollowingController extends Controller
{


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $user)
    {
        auth()->user()->following()->toggle($user);
        return redirect()->back();
    }
}
