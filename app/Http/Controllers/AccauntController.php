<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccauntController extends Controller
{


    /**
     * Display the specified resource.
     */
    public function show(User $account)
    {

        $user = $account;
        $posts = Post::where('user_id', $account->id)->get();
        $count = count($posts);
        $userone = User::find($account->id);
        $following = $userone->following->count();
        $follower = Follower::find($account->id);
        $follower?$users = $follower->followers->count(): $users = 0 ;
        return view('accounts.show', compact('posts', 'user', 'count', 'users', 'following' ));

    }

}
