<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PostsCommentsController extends Controller
{

    public function store(Request $request, Post $post)
    {

        if (! Gate::allows('store-comment', $post)) {
            return redirect()->back()->with('success', 'Вы не можете оставить коментарий');
        }
        $validated = $request->validate([
            'comment' => 'required', 'min:3', 'max:2048',
            'user_id' => 'required', 'int',
        ]);

        $validated['post_id'] = $post->id;
        Comment::create($validated);
        return redirect()->back()->with('success', 'комментарий будет отображен только после апрува администратором');
    }

    public function destroy(Post $post,Comment $comment)
    {
        $this->authorize('delete-comment', $comment);
        $comment->delete();
        return redirect()->back()->with('success', 'deleted');
    }
}
