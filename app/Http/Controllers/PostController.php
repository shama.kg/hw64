<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index()
    {

        $posts = Post::all();
        return view('posts.index', compact('posts'));

    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function show(Post $post)
    {


        $comments = Post::find($post->id)->comments->where('approve', '1');

        return view('posts.show', compact('post','comments'));
    }
    public function store(Request $request)
    {
      $validated = $request->validate([
              'image' => 'required|image',
              'description' => 'required|min:2|max:2048',
                'user_id' => 'required|min:1|max:255|int'
          ]);
        $picture = data_get($validated, 'image');
        if ($picture) {
            $path = $picture->store('image', 'public');
            $validated['image'] = $path;
        }
     $account = $validated['user_id'] ;
        Post::create($validated);
        return redirect()->route('account.show', compact('account'))->with('status', 'фото добавлено');

    }

    public function create(User $user)
    {
        $this->authorize('create-post', $user);
//        $user = $request->user;
        return view('accounts.create', compact('user'));
    }
}
