<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Policies\PostPolicy;
use http\Env\Request;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
//        Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('delete-comment', function (User $users , Comment $comment) {
            return $users->id == $comment->user_id;
        });

        Gate::define('store-comment', function (User $users , Post $post) {
            return $users->id != $post->user_id;
        });

        Gate::define('create-post', function (User $users,  User $user ) {
            return $users->id == $user->id;
        });

    }
}
