<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\AccauntController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PostController::class , 'index'])->middleware('auth')->name('home');

Route::resource('posts', PostController::class)->middleware('auth')->except('create');
Route::get('posts/{user}/create', [PostController::class, 'create'])->middleware('auth')->name('posts.create');
Route::resource('account', AccauntController::class)->middleware('auth');
Route::resource('posts.comments', \App\Http\Controllers\PostsCommentsController::class)->middleware('auth');
Route::resource('posts.likes', \App\Http\Controllers\PostsLikesContoller::class)->middleware('auth');
Route::resource('account.following', \App\Http\Controllers\AccauntsFollowingController::class)->middleware('auth');

Auth::routes();
