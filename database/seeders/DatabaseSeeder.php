<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Follower;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {


         $users = User::factory(10)->create();
        foreach ($users as $user) {
            Follower::factory()->state(['user_id' => $user->id])->create();
        }
        $followers = Follower::all();
        foreach ($users as $user) {
            $user->following()->attach(
                $followers
            );
        }
        $this->call(PostSeeder::class);
        $this->call(CommentSeeder::class);





        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
