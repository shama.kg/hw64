<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'image'=>$this->getImage(rand(1,4)),
            'description'=>$this->faker->text(200),
            'user_id'=>rand(1,10),
            'like'=> rand(1,100)
        ];
    }
    private function getImage($image_number = 1): string

    {
        $path = storage_path() . "/seed_pictures/" . $image_number . ".jpeg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpeg');
        Storage::disk('public')->put('pictures/'.$image_name, $resize->__toString());
        return 'pictures/'.$image_name;

    }
}
